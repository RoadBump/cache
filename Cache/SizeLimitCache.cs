﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bruss
{
    public class SizeLimitCache<TKey, TValue> : CacheBase<TKey, TValue>
    {
        public SizeLimitCache(IRanker ranker)
            : this(int.MaxValue, ranker)
        { }

        public SizeLimitCache(int maxSize, IRanker ranker)
        {
            if (ranker == null) throw new ArgumentNullException("ranker");
            MaxSize = maxSize;
            _ranker = ranker;
        }

        readonly IRanker _ranker;
        protected override IRanker Ranker
        {
            get { return _ranker; }
        }

        public override bool Add(TKey key, TValue value)
        {
            return Add(new CacheItem(key, value, Ranker.CreateRank()));
        }

        public override bool Add(TKey key, TValue value, float rank)
        {
            return Add(new CacheItem(key, value, Ranker.CreateRank(rank)));
        }

        int _maxSize;
        public int MaxSize
        {
            get { return _maxSize; }
            set
            {
                if (MaxSize < 0) throw new ArgumentOutOfRangeException("value");
                _maxSize = value;
                if (Count > _maxSize) RemoveBottom(Count - _maxSize);
            }
        }

        new bool Add(CacheItem item)
        {
            if (!ContainsKey(item.Key))
            {
                if (Count < MaxSize ||
                    Count > 0 && item.Rank.Rank >= EnumerateByRank().First().Rank.Rank)
                {
                    base.Add(item);
                    if (Count > MaxSize) RemoveBottom(Count - MaxSize);
                    return true;
                }
            }
            return false;
        }
    }
}
