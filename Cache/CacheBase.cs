﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bruss
{
    public abstract class CacheBase<TKey, TValue> : IWrappedCache<TKey, TValue>
    {
        const int Key = 0, Rank = 1;

        static IComparer<TKey> _comparer = Comparer<TKey>.Default;

        CacheItem _keyTree;

        CacheItem _rankTree;

        int _count;

        int _stamp;

        protected virtual void OnItemRequested(CacheItem item, DateTime requestTime)
        {
            if (item.Parent[Key] == null && item != _keyTree)//not in collection
                throw new ArgumentException("The item is not in collection.", "item");

            Ranker.OnRequested(item.Rank, requestTime);
            //reinsert rank
            Remove(item, Rank);
            InsertRank(item);
        }

        protected virtual void OnItemsRemoved(IEnumerable<CacheItem> items, int count)
        {
            if (ItemsRemoved != null)
            {
                var itms = items.Select(i => new KeyValuePair<TKey, TValue>(i.Key, i.Value));
                ItemsRemoved(this, new ItemsRemovedEventArgs<TKey, TValue>(itms, count));
            }
        }

        protected void RankChanged(CacheItem item)
        {
            if (item.Parent[Rank] == null && item != _rankTree)//not in collection
                throw new ArgumentException("The item is not in collection.", "item");
            //reinsert rank
            Remove(item, Rank);
            InsertRank(item);
        }

        protected void Add(CacheItem item)
        {
            if (item == null) throw new ArgumentNullException("item");

            CacheItem parent = GetSelfOrParent(item.Key);
            if (parent != null)
            {
                if (item.Key.Equals(parent.Key))
                    throw new InvalidOperationException();

                if (_comparer.Compare(item.Key, parent.Key) > 0)
                    parent.Right[Key] = item;
                else
                    parent.Left[Key] = item;
                item.Parent[Key] = parent;
            }
            else//root node
                _keyTree = item;

            InsertRank(item);

            _count++;
            _stamp++;
        }

        protected bool Remove(float minRank)
        {
            CacheItem node = _rankTree;
            CacheItem removedNode = null;
            int removed = 0;

            while (node != null)
            {
                if (node.Rank.Rank >= minRank)
                    node = node.Left[Rank];
                else
                {
                    CacheItem right = node.Right[Rank];

                    if (node.Parent[Rank] != null)
                        node.Parent[Rank].Left[Rank] = right;
                    else
                        _rankTree = right;

                    if (right != null) right.Parent[Rank] = node.Parent[Rank];

                    node.Parent[Rank] = node.Right[Rank] = null;

                    foreach (CacheItem n in Enumerate(node, Rank, true))
                    {
                        Remove(n, Key);
                        removed++;
                    }
                    //concatenate all removed nodes to one enumerable tree
                    if (removedNode != null)
                    {
                        node.Parent[Rank] = removedNode;
                        removedNode.Right[Rank] = node;
                    }
                    removedNode = node;
                    //continue searching in right subtree
                    node = right;
                }
            }

            _count -= removed;
            if (removed > 0)
            {
                _stamp++;
                while (removedNode.Parent[Rank] != null)
                    removedNode = removedNode.Parent[Rank];
                OnItemsRemoved(Enumerate(removedNode, Rank, false), removed);
            }

            return removed > 0;
        }

        protected void Remove(CacheItem[] items)
        {
            CacheItem[] removedItems = new CacheItem[items.Length];
            int removed = 0;
            foreach (var item in items)
                if (item.Parent[Key] != null || item == _keyTree)//in collection
                {
                    Remove(item, Key);
                    Remove(item, Rank);
                    removedItems[removed++] = item;
                }

            _count -= removed;
            if (removed > 0)
            {
                _stamp++;
                if (removed != removedItems.Length) Array.Resize(ref removedItems, removed);
                OnItemsRemoved(items, removed);
            }
        }

        protected CacheItem GetItem(TKey key)
        {
            CacheItem item = GetSelfOrParent(key);
            if (item != null && item.Key.Equals(key)) return item;
            return null;
        }

        protected bool RemoveBottom(int bottomN)
        {
            CacheItem[] removedNodes = new CacheItem[bottomN];
            int removed = 0;
            var enumerator = EnumerateByRank().GetEnumerator();

            for (; removed < bottomN && enumerator.MoveNext(); removed++)
                removedNodes[removed] = enumerator.Current;

            for (int i = 0; i < removed; i++)
            {
                Remove(removedNodes[i], Key);
                Remove(removedNodes[i], Rank);
            }

            _count -= removed;
            if (removed > 0)
            {
                _stamp++;
                OnItemsRemoved(removedNodes, removed);
            }

            return removed > 0;
        }

        protected abstract IRanker Ranker { get; }

        protected IEnumerable<CacheItem> EnumerateByKey()
        {
            return Enumerate(_keyTree, Key, true);
        }

        protected IEnumerable<CacheItem> EnumerateByRank()
        {
            return Enumerate(_rankTree, Rank, true);
        }

        protected IEnumerable<CacheItem> EnumerateByKeyReversed()
        {
            return EnumerateReverse(_keyTree, Key, true);
        }

        protected IEnumerable<CacheItem> EnumerateByRanlReserved()
        {
            return EnumerateReverse(_rankTree, Rank, true);
        }

        private CacheItem GetSelfOrParent(TKey key)
        {
            if (_keyTree == null) return null;

            CacheItem node = _keyTree;
            int cmp;
            for (; ; )
            {
                cmp = _comparer.Compare(key, node.Key);
                if (cmp > 0)
                {
                    if (node.Right[Key] == null) return node;
                    node = node.Right[Key];
                }
                else if (cmp < 0)
                {
                    if (node.Left[Key] == null) return node;
                    node = node.Left[Key];
                }
                else
                    return node;
            }
        }

        private void InsertRank(CacheItem item)
        {
            if (_rankTree == null) _rankTree = item;
            else
            {
                //insert at appropriate location
                float rank = item.Rank.Rank;
                CacheItem current = _rankTree;
                for (; ; )
                {
                    if (rank >= current.Rank.Rank)
                    {
                        if (current.Right[Rank] == null)
                        {
                            current.Right[Rank] = item;
                            break;
                        }
                        else
                            current = current.Right[Rank];
                    }
                    else
                    {
                        if (current.Left[Rank] == null)
                        {
                            current.Left[Rank] = item;
                            break;
                        }
                        else
                            current = current.Left[Rank];
                    }
                }
                item.Parent[Rank] = current;
            }
        }

        private void Remove(CacheItem item)
        {
            Remove(item, Key);
            Remove(item, Rank);

            _count--;
            _stamp++;
            OnItemsRemoved(new CacheItem[] { item }, 1);
        }

        private void Remove(CacheItem item, int dim)
        {
            CacheItem child = item.Left[dim];
            if (item.Right[dim] != null)
            {
                if (child != null)
                {
                    //both children exist, add right as the right most of left
                    CacheItem tmp = child;
                    while (tmp.Right[dim] != null)
                        tmp = tmp.Right[dim];
                    item.Right[dim].Parent[dim] = tmp;
                    tmp.Right[dim] = item.Right[dim];
                }
                else
                    child = item.Right[dim];
            }

            if (item.Parent[dim] != null)
            {
                if (item == item.Parent[dim].Left[dim]) item.Parent[dim].Left[dim] = child;
                else item.Parent[dim].Right[dim] = child;
            }
            else//root
            {
                if (dim == Key) _keyTree = child;
                else _rankTree = child;
            }

            if (child != null) child.Parent[dim] = item.Parent[dim];

            item.Parent[dim] = item.Left[dim] = item.Right[dim] = null;
        }

        IEnumerable<CacheItem> Enumerate(CacheItem node, int dim, bool sync)
        {
            if (node != null)
            {
                CacheItem root = node.Parent[dim];
                int stamp = _stamp;

                while (node != root)
                {
                    //start at the left most node in subtree
                    while (node.Left[dim] != null)
                        node = node.Left[dim];

                    do
                    {
                        if (sync && stamp != _stamp) throw new InvalidOperationException();
                        yield return node;

                        if (node.Right[dim] != null)
                        {
                            //check it's subtree
                            node = node.Right[dim];
                            break;
                        }
                        //go to parent
                        while (node.Parent[dim] != root && node == node.Parent[dim].Right[dim])
                            node = node.Parent[dim];//it's right child, skip already-visited parent
                        node = node.Parent[dim];
                    } while (node != root);
                }
            }
        }

        IEnumerable<CacheItem> EnumerateReverse(CacheItem node, int dim, bool sync)
        {
            if (node != null)
            {
                CacheItem root = node.Parent[dim];
                int stamp = _stamp;

                while (node != root)
                {
                    //start at the right most node in subtree
                    while (node.Right[dim] != null)
                        node = node.Right[dim];

                    do
                    {
                        if (sync && stamp != _stamp) throw new InvalidOperationException();
                        yield return node;

                        if (node.Left[dim] != null)
                        {
                            //check it's subtree
                            node = node.Left[dim];
                            break;
                        }
                        //go to parent
                        while (node.Parent[dim] != root && node == node.Parent[dim].Left[dim])
                            node = node.Parent[dim];//it's left child, skip already-visited parent
                        node = node.Parent[dim];
                    } while (node != root);
                }
            }
        }

        #region IWrappedCache<Tkey, TValue> Members

        bool IWrappedCache<TKey, TValue>.RemoveBottom(int bottomN)
        {
            return this.RemoveBottom(bottomN);
        }

        float IWrappedCache<TKey, TValue>.GetBottomRank()
        {
            if (_count == 0) throw new InvalidOperationException();
            CacheItem node = _rankTree;
            while (node.Left[Rank] != null)
                node = node.Left[Rank];
            return node.Rank.Rank;
        }

        IRanker IWrappedCache<TKey, TValue>.Ranker
        {
            get { return this.Ranker; }
        }

        #endregion

        #region ICache<TKey,TValue> Members

        public virtual bool Add(TKey key, TValue value)
        {
            if (!ContainsKey(key))
            {
                IRank rank = Ranker.CreateRank();
                Add(new CacheItem(key, value, rank));
                return true;
            }
            return false;//shall i throw an error on duplicate key?
        }

        public virtual bool Add(TKey key, TValue value, float rank)
        {
            if (!ContainsKey(key))
            {
                IRank irank = Ranker.CreateRank(rank);
                Add(new CacheItem(key, value, irank));
                return true;
            }
            return false;//shall i throw an error?
        }

        public virtual void Clean()
        {
        }

        public void Clean(float minRank)
        {
            Remove(minRank);
        }

        public bool Examine(TKey key, out TValue value)
        {
            CacheItem item = GetItem(key);
            if (item != null)
            {
                value = item.Value;
                return true;
            }
            value = default(TValue);
            return false;
        }

        public event EventHandler<ItemsRemovedEventArgs<TKey, TValue>> ItemsRemoved;

        #endregion

        #region IDictionary<TKey,TValue> Members

        void IDictionary<TKey, TValue>.Add(TKey key, TValue value)
        {
            if (ContainsKey(key)) throw new InvalidOperationException();
            Add(key, value);
        }

        public bool ContainsKey(TKey key)
        {
            CacheItem item = GetSelfOrParent(key);
            return item != null && item.Key.Equals(key);
        }

        SelectorCollection<TKey> _keys;
        public ICollection<TKey> Keys
        {
            get
            {
                if (_keys == null)
                    _keys = new SelectorCollection<TKey>(this, ci => ci.Key, ContainsKey);
                return _keys;
            }
        }

        public bool Remove(TKey key)
        {
            CacheItem item = GetItem(key);
            if (item != null) Remove(item);
            return item != null;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            DateTime time = DateTime.Now;
            CacheItem item = GetItem(key);
            if (item != null)
            {
                OnItemRequested(item, time);
                value = item.Value;
                return true;
            }
            value = default(TValue);
            return false;
        }

        SelectorCollection<TValue> _values;
        public ICollection<TValue> Values
        {
            get
            {
                if (_values == null)
                    _values = new SelectorCollection<TValue>(this, ci => ci.Value, null);
                return _values;
            }
        }

        public TValue this[TKey key]
        {
            get
            {
                DateTime time = DateTime.Now;
                CacheItem item = GetItem(key);

                if (item == null) throw new KeyNotFoundException();

                OnItemRequested(item, time);
                return item.Value;
            }
            set
            {
                throw new NotSupportedException();
            }
        }

        #endregion

        #region ICollection<KeyValuePair<TKey,TValue>> Members

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            ((IDictionary<TKey, TValue>)this).Add(item.Key, item.Value);
        }

        public virtual void Clear()
        {
            var enumerable = Enumerate(_keyTree, Key, false);
            _keyTree = null;
            _rankTree = null;
            int count = _count;
            _count = 0;
            _stamp++;
            OnItemsRemoved(enumerable, count);
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            CacheItem cacheItem = GetSelfOrParent(item.Key);
            return cacheItem != null && cacheItem.Key.Equals(item.Key) && object.Equals(item.Value, cacheItem.Value);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            if (array == null) throw new ArgumentNullException("array");
            if (arrayIndex < 0 || arrayIndex + _count > array.Length) throw new ArgumentOutOfRangeException("arrayIndex");

            foreach (CacheItem item in EnumerateByKey())
                array[arrayIndex++] = new KeyValuePair<TKey, TValue>(item.Key, item.Value);
        }

        public int Count
        {
            get { return _count; }
        }

        public bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            CacheItem cItem = GetSelfOrParent(item.Key);
            if (cItem != null && cItem.Key.Equals(item.Key) && object.Equals(item.Value, cItem.Value))
            {
                Remove(cItem);
                return true;
            }
            return false;
        }

        #endregion

        #region IEnumerable<KeyValuePair<TKey,TValue>> Members

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return Enumerate(_keyTree, Key, true)
                .Select(ci => new KeyValuePair<TKey, TValue>(ci.Key, ci.Value))
                .GetEnumerator();
        }

        public IEnumerable<KeyValuePair<TKey, TValue>> Reverse()
        {
            return EnumerateReverse(_keyTree, Key, true)
                .Select(ci => new KeyValuePair<TKey, TValue>(ci.Key, ci.Value));
        }

        //TODO:Min,Max

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        protected class CacheItem
        {
            public CacheItem(TKey key, TValue value, IRank rank)
            {
                if (key == null) throw new ArgumentNullException("key");
                if (rank == null) throw new ArgumentNullException("rank");
                _key = key;
                _value = value;
                _rank = rank;
            }

            readonly TKey _key;
            public TKey Key
            {
                get { return _key; }
            }

            readonly TValue _value;
            public TValue Value
            {
                get { return _value; }
            }

            IRank _rank;
            public IRank Rank
            {
                get { return _rank; }
            }

            #region Internal tree

            internal readonly CacheItem[] Parent = new CacheItem[2];

            internal readonly CacheItem[] Right = new CacheItem[2];

            internal readonly CacheItem[] Left = new CacheItem[2];

            #endregion
        }

        class SelectorCollection<T> : ICollection<T>
        {
            readonly CacheBase<TKey, TValue> _owner;

            readonly Func<CacheItem, T> _selector;

            readonly Func<T, bool> _customContains;

            public SelectorCollection(CacheBase<TKey, TValue> owner, Func<CacheItem, T> selector, Func<T, bool> customContains)
            {
                _owner = owner;
                _selector = selector;
                _customContains = customContains;
            }

            #region ICollection<T> Members

            public void Add(T item)
            {
                throw new NotSupportedException();
            }

            public void Clear()
            {
                throw new NotSupportedException();
            }

            public bool Contains(T item)
            {
                if (_customContains != null) return _customContains(item);
                return _owner.EnumerateByKey().Select(_selector).Contains(item);
            }

            public void CopyTo(T[] array, int arrayIndex)
            {
                if (array == null) throw new ArgumentNullException("array");
                if (arrayIndex < 0 || arrayIndex + Count > array.Length) throw new ArgumentOutOfRangeException("arrayIndex");

                foreach (CacheItem item in _owner.EnumerateByKey())
                    array[arrayIndex++] = _selector(item);
            }

            public int Count
            {
                get { return _owner.Count; }
            }

            public bool IsReadOnly
            {
                get { return true; }
            }

            public bool Remove(T item)
            {
                throw new NotSupportedException();
            }

            #endregion

            #region IEnumerable<T> Members

            public IEnumerator<T> GetEnumerator()
            {
                return _owner.EnumerateByKey().Select(_selector).GetEnumerator();
            }

            #endregion

            #region IEnumerable Members

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            #endregion
        }
    }
}
