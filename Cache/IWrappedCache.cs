﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bruss
{
    /// <summary>
    /// Interface for caches that enable wrapping mechanism. Provides access to some members required for wrapping.
    /// </summary>
    internal interface IWrappedCache<TKey, TValue> : ICache<TKey, TValue>
    {
        /// <summary>
        /// Removes bottom N items from the cache. Required by size-limited cache wrappers.
        /// </summary>
        /// <param name="bottomN">The number of items to remove.</param>
        /// <returns>True if one or more items were removed, otherwise false.</returns>
        bool RemoveBottom(int bottomN);

        /// <summary>
        /// Returns the botton item's rank. Required by time-limited cache wrappers.
        /// </summary>
        /// <returns>The rank of the bottom item, that is the lowest rank in the collection.</returns>
        /// <exception cref="System.InvalidOperationException">The collection is empty.</exception>
        float GetBottomRank();

        /// <summary>
        /// Returns the object used by this instance for ranking items. Required by time-limited cache wrappers.
        /// </summary>
        IRanker Ranker { get; }
    }
}
