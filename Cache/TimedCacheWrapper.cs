﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bruss
{
    public class TimedCacheWrapper<TKey, TValue> : CacheWrapper<TKey, TValue>, ITimedCache<TKey, TValue>
    {
        static readonly TimeSpan MaxTimerValue = TimeSpan.FromMilliseconds(4294967294);

        internal TimedCacheWrapper(IWrappedCache<TKey, TValue> wrapped)
            : base(wrapped)
        {
            _timer = new System.Threading.Timer(TimerCallback);
            Wrapped.ItemsRemoved += OnItemsRemoved;
        }

        new ITimedRanker Ranker
        {
            get
            {
                ITimedRanker ranker = base.Ranker as ITimedRanker;
                if (ranker == null) throw new InvalidOperationException("The ranker of the wrapped object must inherit from ITimedRanker.");
                return ranker;
            }
        }

        bool _autoClean;
        public bool AutoClean
        {
            get { return _autoClean; }
            set
            {
                if (value != _autoClean)
                {
                    _autoClean = value;
                    SetTimer();
                }
            }
        }

        public override void Clean()
        {
            Clean(DateTime.Now);
        }

        System.Threading.Timer _timer;
        DateTime deathTime;
        void SetTimer()
        {
            if (_autoClean && Wrapped.Count > 0)
            {
                DateTime dt = Ranker.Translate(Wrapped.GetBottomRank());
                if (dt < deathTime) deathTime = dt;

                TimeSpan span = deathTime - DateTime.Now;
                if (span < TimeSpan.Zero) span = TimeSpan.Zero;
                else if (span > MaxTimerValue) span = MaxTimerValue;

                _timer.Change(span, TimeSpan.FromMilliseconds(-1));
            }
            else
                _timer.Change(-1, -1);
        }

        void TimerCallback(object state)
        {
            _timer.Change(-1, -1);
            lock (this)
            {
                Clean(DateTime.Now);
                SetTimer();
            }
        }

        void OnItemsRemoved(object sender, ItemsRemovedEventArgs<TKey, TValue> e)
        {
            SetTimer();
        }

        #region ITimedCache<TKey,TValue> Members

        public bool Add(TKey key, TValue value, TimeSpan timeToLive)
        {
            DateTime time = DateTime.Now;
            if (_autoClean && timeToLive < TimeSpan.Zero) return false;
            lock (this)
            {
                bool val = Wrapped.Add(key, value, Ranker.Translate(time + timeToLive));
                if (val) SetTimer();
                return val;
            }
        }

        public void Clean(DateTime forTime)
        {
            lock (this) { Wrapped.Clean(Ranker.Translate(forTime)); }
        }

        #endregion

        #region ICache<TKey,TValue> Members

        public override bool Add(TKey key, TValue value)
        {
            lock (this)
            {
                bool b = base.Add(key, value);
                if (b) SetTimer();
                return b;
            }
        }

        public override bool Add(TKey key, TValue value, float rank)
        {
            lock (this)
            {
                bool b = base.Add(key, value, rank);
                if (b) SetTimer();
                return b;
            }
        }

        public override void Clean(float minRank)
        {
            lock (this)
            {
                base.Clean(minRank);
            }
        }

        public override bool Examine(TKey key, out TValue value)
        {
            lock (this)
            {
                return base.Examine(key, out value);
            }
        }

        #endregion

        #region IDictionary<TKey,TValue> Members

        void IDictionary<TKey, TValue>.Add(TKey key, TValue value)
        {
            lock (this)
            {
                ((IDictionary<TKey, TValue>)Wrapped).Add(key, value);
                SetTimer();
            }
        }

        public override bool ContainsKey(TKey key)
        {
            lock (this)
            {
                return base.ContainsKey(key);
            }
        }

        public override ICollection<TKey> Keys
        {
            get
            {
                lock (this)
                {
                    return base.Keys;
                }
            }
        }

        public override bool Remove(TKey key)
        {
            lock (this)
            {
                return base.Remove(key);
            }
        }

        public override bool TryGetValue(TKey key, out TValue value)
        {
            lock (this)
            {
                return base.TryGetValue(key, out value);
            }
        }

        public override ICollection<TValue> Values
        {
            get
            {
                lock (this)
                {
                    return base.Values;
                }
            }
        }

        public override TValue this[TKey key]
        {
            get
            {
                lock (this)
                {
                    return base[key];
                }
            }
            set
            {
                lock (this)
                {
                    base[key] = value;
                }
            }
        }

        #endregion

        #region ICollection<KeyValuePair<TKey,TValue>> Members

        public override void Add(KeyValuePair<TKey, TValue> item)
        {
            lock (this)
            {
                base.Add(item);
            }
        }

        public override void Clear()
        {
            lock (this)
            {
                base.Clear();
            }
        }

        public override bool Contains(KeyValuePair<TKey, TValue> item)
        {
            lock (this)
            {
                return base.Contains(item);
            }
        }

        public override void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            lock (this)
            {
                base.CopyTo(array, arrayIndex);
            }
        }

        public override int Count
        {
            get
            {
                lock (this)
                {
                    return base.Count;
                }
            }
        }

        public override bool Remove(KeyValuePair<TKey, TValue> item)
        {
            lock (this)
            {
                return base.Remove(item);
            }
        }

        #endregion

        #region IEnumerable<KeyValuePair<TKey,TValue>> Members

        public override IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            lock (this)
            {
                return base.GetEnumerator();
            }
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            lock (this)
            {
                return ((System.Collections.IEnumerable)Wrapped).GetEnumerator();
            }
        }

        #endregion
    }
}
