﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bruss
{
    public class TimedCache<TKey, TValue> : SizeLimitCache<TKey, TValue>,ITimedCache<TKey,TValue>
    {
        ITimedRanker _ranker;

        public TimedCache(ITimedRanker ranker) : base(ranker) { throw new NotImplementedException(); }

        public TimedCache(ITimedRanker ranker, int maxSize) : base(maxSize,ranker) { throw new NotImplementedException(); }

        public TimedCache(ITimedRanker ranker, bool autoClean) : base(ranker) { throw new NotImplementedException(); }

        public TimedCache(ITimedRanker ranker, int maxSize, bool autoClean) : base( maxSize,ranker) { throw new NotImplementedException(); }

        public bool AutoClean
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public override void Clean()
        {
            Clean(DateTime.Now);
        }

        #region ITimedCache<TKey,TValue> Members

        public bool Add(TKey key, TValue value, TimeSpan timeToLive)
        {
            return Add(key, value, _ranker.Translate(DateTime.Now + timeToLive));
        }

        public void Clean(DateTime forTime)
        {
            Clean(_ranker.Translate(forTime));
        }

        #endregion

        void SetTimer()
        {
            
        }

        //#region IDictionary<TKey,TValue> Members

        //void IDictionary<TKey, TValue>.Add(TKey key, TValue value)
        //{
        //    ((IDictionary<TKey, TValue>)base).Add(key, value);
        //}

        //#endregion

        //#region IEnumerable Members

        //public new System.Collections.IEnumerator GetEnumerator()
        //{
        //    throw new NotImplementedException();
        //}

        //#endregion
    }
}
