﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bruss
{
    public class StorageLimitCache<TKey, TValue> : CacheBase<TKey, TValue>
    {
        public StorageLimitCache(IRanker ranker, int maxVolume)
        {
            if (ranker == null) throw new ArgumentNullException("ranker");
            MaxVolume = maxVolume;
            _ranker = ranker;
        }

        public StorageLimitCache(IRanker ranker)
            : this(ranker, int.MaxValue)
        {
        }

        public bool Add(TKey key, TValue value, int volume)
        {
            var item = new StorageLimitCacheItem(key, value, Ranker.CreateRank(), volume);
            return Add(item);
        }

        public bool Add(TKey key, TValue value, float rank, int volume)
        {
            var item = new StorageLimitCacheItem(key, value, Ranker.CreateRank(rank), volume);
            return Add(item);
        }

        public override bool Add(TKey key, TValue value)
        {
            var item = new StorageLimitCacheItem(key, value, Ranker.CreateRank());
            return Add(item);
        }

        public override bool Add(TKey key, TValue value, float rank)
        {
            var item = new StorageLimitCacheItem(key, value, Ranker.CreateRank(rank));
            return Add(item);
        }

        int _maxVolume;
        public int MaxVolume
        {
            get { return _maxVolume; }
            set
            {
                if (value < 0) throw new ArgumentOutOfRangeException("value");
                _maxVolume = value;
                if (Volume > _maxVolume)
                    base.Remove(KnapOut(_maxVolume));
            }
        }

        int _volume;
        public int Volume
        {
            get { return _volume; }
        }

        readonly IRanker _ranker;
        protected override IRanker Ranker
        {
            get { return _ranker; }
        }

        bool Add(StorageLimitCacheItem item)
        {
            if (item.Volume < MaxVolume)
            {
                base.Add(item);
                _volume += item.Volume;
                CacheItem[] removed = KnapOut(MaxVolume);
                bool added = !removed.Contains(item);
                base.Remove(removed);
                return added;
            }
            return false;
        }

        protected override void OnItemsRemoved(IEnumerable<CacheBase<TKey, TValue>.CacheItem> items, int count)
        {
            foreach (var item in items.Cast<StorageLimitCacheItem>())
                _volume -= item.Volume;

            base.OnItemsRemoved(items, count);
        }

        CacheItem[] KnapOut(int desiredVolume)
        {
            if (_volume > desiredVolume)
            {
                var items = base.EnumerateByKey().Cast<StorageLimitCacheItem>().ToArray();
                Array.Sort(items, (x, y) => y.Volume - x.Volume);
                float[,] T = Knapsack(items, _volume - desiredVolume);
                //find which items were thrown out
                List<CacheItem> removedItems = new List<CacheItem>();
                int i = T.GetLength(0) - 1,
                    w = T.GetLength(1) - 1;
                for (; i > 0; i--)
                {
                    if (T[i, w] < T[i - 1, w])
                    {
                        removedItems.Add(items[i - 1]);
                        //this item is built upon the item in T[i-1, w-item.Volume]
                        w -= items[i - 1].Volume;
                        if (w <= 0)//no more items are left
                            break;
                    }
                }

                return removedItems.ToArray();
            }
            return new CacheItem[0];
        }

        float[,] Knapsack(StorageLimitCacheItem[] items, int deviation)
        {
            //find the minimum possible rank that needs to be cleared out
            //find a combination of items whose volume >= deviation and rank = minimum
            float[,] T = new float[items.Length + 1, deviation + 1];

            for (int i = 1; i <= deviation; i++)
                T[0, i] = float.PositiveInfinity;

            for (int i = 1; i <= items.Length; i++)
            {
                var item = items[i - 1];
                for (int w = 1; w <= deviation; w++)
                {
                    float with;
                    if (item.Volume >= w)
                        with = item.Rank.Rank;
                    else
                        with = T[i - 1, w - item.Volume] + item.Rank.Rank;

                    T[i, w] = Math.Min(with, T[i - 1, w]);
                }
            }

            return T;
        }

        protected class StorageLimitCacheItem : CacheItem
        {
            public StorageLimitCacheItem(TKey key, TValue value, IRank rank, int volume)
                : base(key, value, rank)
            {
                if (volume < 1) throw new ArgumentOutOfRangeException("volume");
                _volume = volume;
            }

            public StorageLimitCacheItem(TKey key, TValue value, IRank rank)
                : base(key, value, rank)
            { }

            int _volume = 1;
            public int Volume
            {
                get { return _volume; }
            }
        }
    }
}
