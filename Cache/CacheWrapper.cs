﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bruss
{
    public abstract class CacheWrapper<TKey, TValue> : IWrappedCache<TKey, TValue>
    {
        internal CacheWrapper(IWrappedCache<TKey, TValue> wrapped)
        {
            if (wrapped == null) throw new ArgumentNullException("wrapped");
            _wrapped = wrapped;
        }

        IWrappedCache<TKey, TValue> _wrapped;
        internal IWrappedCache<TKey, TValue> Wrapped
        {
            get { return _wrapped; }
        }

        #region IWrappedCache<TKey, TValue> Members

        bool IWrappedCache<TKey, TValue>.RemoveBottom(int bottomN)
        {
            return RemoveBottom(bottomN);
        }

        float IWrappedCache<TKey, TValue>.GetBottomRank()
        {
            return GetBottomRank();
        }

        IRanker IWrappedCache<TKey, TValue>.Ranker
        {
            get { return Ranker; }
        }

        protected virtual bool RemoveBottom(int bottomN)
        {
            return _wrapped.RemoveBottom(bottomN);
        }

        protected virtual float GetBottomRank()
        {
            return _wrapped.GetBottomRank();
        }

        protected virtual IRanker Ranker
        {
            get { return _wrapped.Ranker; }
        }

        #endregion

        #region ICache<TKey,TValue> Members

        public virtual bool Add(TKey key, TValue value)
        {
            return _wrapped.Add(key, value);
        }

        public virtual bool Add(TKey key, TValue value, float rank)
        {
            return _wrapped.Add(key, value, rank);
        }

        public virtual void Clean()
        {
            _wrapped.Clean();
        }

        public virtual void Clean(float minRank)
        {
            _wrapped.Clean(minRank);
        }

        public virtual bool Examine(TKey key, out TValue value)
        {
            return _wrapped.Examine(key, out value);
        }

        public event EventHandler<ItemsRemovedEventArgs<TKey, TValue>> ItemsRemoved
        {
            add { _wrapped.ItemsRemoved += value; }
            remove { _wrapped.ItemsRemoved -= value; }
        }

        #endregion

        #region IDictionary<TKey,TValue> Members

        void IDictionary<TKey, TValue>.Add(TKey key, TValue value)
        {
            ((IDictionary<TKey, TValue>)_wrapped).Add(key, value);
        }

        public virtual bool ContainsKey(TKey key)
        {
            return _wrapped.ContainsKey(key);
        }

        public virtual ICollection<TKey> Keys
        {
            get { return _wrapped.Keys; }
        }

        public virtual bool Remove(TKey key)
        {
            return _wrapped.Remove(key);
        }

        public virtual bool TryGetValue(TKey key, out TValue value)
        {
            return _wrapped.TryGetValue(key, out value);
        }

        public virtual ICollection<TValue> Values
        {
            get { return _wrapped.Values; }
        }

        public virtual TValue this[TKey key]
        {
            get
            {
                return _wrapped[key];
            }
            set
            {
                _wrapped[key] = value;
            }
        }

        #endregion

        #region ICollection<KeyValuePair<TKey,TValue>> Members

        public virtual void Add(KeyValuePair<TKey, TValue> item)
        {
            _wrapped.Add(item);
        }

        public virtual void Clear()
        {
            _wrapped.Clear();
        }

        public virtual bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return _wrapped.Contains(item);
        }

        public virtual void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            _wrapped.CopyTo(array, arrayIndex);
        }

        public virtual int Count
        {
            get { return _wrapped.Count; }
        }

        public virtual bool IsReadOnly
        {
            get { return _wrapped.IsReadOnly; }
        }

        public virtual bool Remove(KeyValuePair<TKey, TValue> item)
        {
            return _wrapped.Remove(item);
        }

        #endregion

        #region IEnumerable<KeyValuePair<TKey,TValue>> Members

        public virtual IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return _wrapped.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return ((System.Collections.IEnumerable)_wrapped).GetEnumerator();
        }

        #endregion
    }
}
