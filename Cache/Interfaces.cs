﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bruss
{
    public interface ICache<TKey, TValue> : IDictionary<TKey, TValue>
    {
        new bool Add(TKey key, TValue value);

        bool Add(TKey key, TValue value, float rank);

        void Clean();//what does this mean? isn't it belong to ITimedCache?

        void Clean(float minRank);

        bool Examine(TKey key, out TValue value);

        event EventHandler<ItemsRemovedEventArgs<TKey, TValue>> ItemsRemoved;
    }

    public interface ITimedCache<TKey, TValue> : ICache<TKey, TValue>
    {
        bool Add(TKey key, TValue value, TimeSpan timeToLive);

        void Clean(DateTime forTime);
    }

    public interface IRank
    {
        float Rank { get; }
    }

    public interface IRanker
    {
        IRank CreateRank();

        IRank CreateRank(float rank);

        void OnRequested(IRank item, DateTime requestTime);
    }

    public interface ITimedRanker : IRanker
    {
        float Translate(DateTime time);

        DateTime Translate(float rank);
    }

    public class ItemsRemovedEventArgs<TKey,TValue> : EventArgs
    {
        public ItemsRemovedEventArgs(IEnumerable<KeyValuePair<TKey, TValue>> items, int count)
        {
            if (count < 0) throw new ArgumentOutOfRangeException("count");
            Items = items;
            Count = count;
        }

        public IEnumerable<KeyValuePair<TKey, TValue>> Items { get; private set; }

        public int Count { get; private set; }
    }
}
