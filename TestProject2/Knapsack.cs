﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestProject2
{
    public static class KnapsackProblem
    {
        public struct Item
        {
            public int Value, Weight;

            public float ValuePerWeight
            {
                get { return (float)Value / Weight; }
            }
        }

        public static int Knapsack(Item[] items, int maxWeight)
        {
            int[,] t = new int[items.Length + 1, maxWeight + 1];

            for (int i = 0; i <= items.Length; i++)
                t[i, 0] = 0;
            for (int i = 0; i <= maxWeight; i++)
                t[0, i] = 0;

            for (int i = 1; i <= items.Length; i++)
                for (int w = 1; w <= maxWeight; w++)
                {
                    Item item = items[i - 1];
                    if (item.Weight > w)
                        t[i, w] = t[i - 1, w];
                    else
                    {
                        int with = t[i - 1, w - item.Weight] + item.Value;
                        int without = t[i - 1, w];
                        t[i, w] = Math.Max(with, without);
                    }
                }

            return t[items.Length, maxWeight];
        }

        static Item[] _items;

        static int?[,] T;

        static int best;

        internal static int steps;//steps the algorithm has taken, for debug purposes

        public static int AdvancedKnapsack(Item[] items, int maxWeight)
        {
            //sort items by value-per-weight
            Array.Sort(items, (x, y) => x.ValuePerWeight.CompareTo(y.ValuePerWeight));

            //make an array for memoization
            T = new int?[items.Length + 1, maxWeight + 1];
            //init
            for (int i = 0; i <= items.Length; i++)
                T[i, 0] = 0;
            for (int i = 0; i <= maxWeight; i++)
                T[0, i] = 0;
            //holds the best result so far
            best = 0;

            _items = items;
            steps = 0;
            return Knap(items.Length, maxWeight, 0).Value;
        }

        static int? Knap(int i, int w, int v)
        {
            if (T[i, w] == null)
            {
                steps++;
                Item item = _items[i - 1];//T is longer by one than the items array
                if (item.Weight > w)
                    T[i, w] = Knap(i - 1, w, v);
                //determine if the item can beat the best result
                //don't process it if not (return null)
                else if (item.ValuePerWeight * w + v > best)
                {
                    int? with = Knap(i - 1, w - item.Weight, v + item.Value) + item.Value;
                    int? without = Knap(i - 1, w, v);
                    T[i, w] = Max(with, without);
                    best = Max(best, T[i, w]).Value;
                }
            }

            return T[i, w];
        }

        static int? Max(int? x, int? y)
        {
            if (x == null) return y;
            if (y == null) return x;
            return x.Value > y.Value ? x : y;
        }

        public static int OppositeKnapsack(Item[] items, int deviation)
        {
            int[,] T = new int[items.Length + 1, deviation + 1];

            for (int i = 1; i <= deviation; i++)
                T[0, i] = int.MaxValue;

            for (int i = 1; i <= items.Length; i++)
            {
                Item item = items[i - 1];
                for (int w = 1; w <= deviation; w++)
                {
                    int with;
                    if (item.Weight > w)
                        with = item.Value;
                    else
                    {
                        with = T[i - 1, w - item.Weight];
                        if (with < int.MaxValue) with += item.Value;
                    }
                    T[i, w] = Math.Min(with, T[i - 1, w]);
                }
            }

            return T[items.Length, deviation];
        }

        public static int MinimalisticKnapsack(Item[] items, int maxWeight)
        {
            int[,] T = new int[items.Length + 1, maxWeight + 1];

            int floor = items.Length - GetLog2(maxWeight);
            if (floor < 0) floor = 0;
            int i, w;

            for (i = 0; i < floor; i++)
                for (w = 1; w <= maxWeight; w++)
                {
                    if (items[i].Weight > w)
                        T[i + 1, w] = T[i, w];
                    else
                        T[i + 1, w] = Math.Max(T[i, w], T[i, w - items[i].Weight] + items[i].Value);
                }


            bool[] M = new bool[items.Length];
            M[items.Length - 1] = true;
            i = items.Length - 2;
            w = maxWeight - items[items.Length - 1].Weight;

            do
            {
                for (; i >= floor; i--)
                    if (items[i].Weight < w)
                    {
                        w -= items[i].Weight;
                        M[i] = true;
                    }
                    else
                        M[i] = false;

                while (!M[i++])
                {
                    if (items[i].Weight > w)
                        T[i + 1, w] = T[i, w];
                    else
                        T[i + 1, w] = Math.Max(T[i, w], T[i, w - items[i].Weight] + items[i].Value);
                }

                M[i - 1] = false;
                w += items[i - 1].Weight;
                i -= 2;
            } while (i < items.Length - 2);

            i++;
            if (items[i].Weight > w)
                T[i + 1, w] = T[i, w];
            else
                T[i + 1, w] = Math.Max(T[i, w], T[i, w - items[i].Weight] + items[i].Value);

            return T[items.Length, maxWeight];
        }

        static int GetLog2(int n)
        {
            //find log2 of the given number, rounded down
            //the highest "on" bit in the number is the answer
            for (int bit = 31; bit >= 0; bit--)
                if ((n & (1 << bit)) != 0)
                    return bit;
            return 0;
        }
    }
}
