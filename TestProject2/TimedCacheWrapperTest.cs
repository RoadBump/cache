﻿using Bruss;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;

namespace TestProject2
{
    [TestClass()]
    public class TimedCacheWrapperTest
    {
        TimedCacheWrapper<string, string> _target;

        [TestInitialize]
        public void Initialize()
        {
            SizeLimitCache<string, string> wrapped = new SizeLimitCache<string, string>(new DummyTimedRanker());
            _target = new TimedCacheWrapper<string, string>(wrapped);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _target.Clear();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void RankerTest()
        {
            //the wrapped cache must have a ITimedRanker
            var a = new TimedCacheWrapper<string, string>(new SizeLimitCache<string, string>(new DummyRanker()));
            a.Add("1", "2", TimeSpan.FromDays(1));
        }

        [TestMethod()]
        public void AutoCleanTest()
        {
            _target.AutoClean = false;
            Assert.IsTrue(_target.Add("-10", null, TimeSpan.FromSeconds(-10)));
            Assert.IsTrue(_target.Add("-5", null, TimeSpan.FromSeconds(-5)));
            Assert.IsTrue(_target.ContainsKey("-10"));
            Assert.IsTrue(_target.ContainsKey("-5"));

            _target.AutoClean = true;
            Thread.Sleep(50);
            Assert.IsFalse(_target.ContainsKey("-10"));
            Assert.IsFalse(_target.ContainsKey("-5"));
            Assert.IsFalse(_target.Add("-1", null, TimeSpan.FromSeconds(-1)));

            Assert.IsTrue(_target.Add("0", null, TimeSpan.Zero));
            Assert.IsTrue(_target.Add("0.5", null, TimeSpan.FromMilliseconds(500)));
            Thread.Sleep(1000);
            Assert.IsFalse(_target.ContainsKey("0"));
            Assert.IsFalse(_target.ContainsKey("0.5"));
        }

        [TestMethod()]
        public void CleanForDateTest()
        {
            _target.AutoClean = false;
            DateTime startTime = DateTime.Now;
            Populate();
            List<int> items = _target.Select(item => int.Parse(item.Key)).ToList();
            int average = (int)items.Average();

            items.RemoveAll(item => item < average);
            items.Sort();

            _target.Clean(startTime + TimeSpan.FromDays(average));
            Assert.IsTrue(items.Select(i => i.ToString())
                .SequenceEqual(_target.Select(i => i.Key)));
        }

        [TestMethod()]
        public void CleanTest()
        {
            _target.AutoClean = false;
            Populate();
            for (int i = 1; i < 100; i++)
            {
                int days = -i;
                Assert.IsTrue(_target.Add(days.ToString(), null, TimeSpan.FromDays(days)));
            }

            List<int> items = _target.Select(i => int.Parse(i.Key, System.Globalization.NumberStyles.AllowLeadingSign))
                .ToList();
            Thread.Sleep(500);
            items.RemoveAll(i => i <= 0);//0 is removed too, because the time advanced since adding

            _target.Clean();
            Assert.IsTrue(_target.Keys.SequenceEqual(items.Select(i => i.ToString())));
        }

        [TestMethod()]
        public void AddTest()
        {
            _target.AutoClean = false;
            DummyTimedRanker ranker = (DummyTimedRanker)((IWrappedCache<string, string>)_target).Ranker;
            Random rnd = new Random();
            for (int i = 0; i < 1000; i++)
            {
                long ms = rnd.Next(int.MinValue, int.MaxValue);
                TimeSpan ttl = TimeSpan.FromMilliseconds(ms);
                DateTime time = DateTime.Now;
                _target.Add(i.ToString(), null, TimeSpan.FromMilliseconds(ms));
                //ignore the precision loss made by converting to float
                ms = (long)((float)ms);
                DateTime expected = time + TimeSpan.FromMilliseconds(ms);
                DateTime actual = ranker.Translate(ranker.LastCreatedRank.Rank);
                TimeSpan diff = (expected - actual).Duration();
                Assert.IsTrue(diff.TotalMilliseconds < 200);
            }
        }

        void Populate()
        {
            int count = DateTime.Now.Millisecond + 5;
            Random rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                int days = rnd.Next(count);
                _target.Add(days.ToString(), null, TimeSpan.FromDays(days));
            }
        }
    }
}
