﻿using Bruss;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System;
using System.Linq;

namespace TestProject2
{
    [TestClass()]
    public class CacheBaseTest : CacheBase<string, string>
    {
        public CacheBaseTest()
        {
            base.ItemsRemoved += OnItemsRemoved;
        }

        [TestCleanup]
        public void Cleanup()
        {
            base.Clear();
            Assert.AreEqual(0, base.Count);
        }

        DummyRanker _ranker = new DummyRanker();
        protected override IRanker Ranker
        {
            get { return _ranker; }
        }

        IEnumerable<KeyValuePair<string, string>> _lastRemovedItems;
        int _lastRemovedCount;
        void OnItemsRemoved(object sender, ItemsRemovedEventArgs<string, string> e)
        {
            _lastRemovedItems = e.Items;
            _lastRemovedCount = e.Count;
        }

        [TestMethod()]
        public void AddTest()
        {
            string key = "key1";
            CacheItem item = CreateCacheItem(key, null);
            base.Add(item);
            Assert.IsTrue(base.ContainsKey(key));
            try
            {
                item = CreateCacheItem(key, "value");
                base.Add(item);
                Assert.Fail();
            }
            catch (InvalidOperationException ex)
            {
                Assert.AreEqual(1, base.Count);
            }
        }

        [TestMethod()]
        public void CleanMinRankTest()
        {
            List<float> d = new List<float>(PopulateDoubles());
            float minRank = d.Average();

            base.Clean(minRank);

            d.RemoveAll(i => i < minRank);
            d.Sort();

            Assert.IsTrue(d.SequenceEqual(EnumerateRank()));
        }

        [TestMethod()]
        public void ClearTest()
        {
            Populate();
            Assert.IsTrue(base.Count > 0);
            base.Clear();
            Assert.AreEqual(0, base.Count);
        }

        [TestMethod()]
        public void ContainsTest()
        {
            string[] keys = GetRandomKeys(25);
            foreach (string key in keys)
                base.Add(CreateCacheItem(key, null));
            foreach (string key in keys)
            {
                Assert.IsTrue(base.Contains(new KeyValuePair<string, string>(key, null)));
                Assert.IsFalse(base.Contains(new KeyValuePair<string, string>(key, "")));
                base.Remove(key);
                Assert.IsFalse(base.Contains(new KeyValuePair<string, string>(key, null)));
            }
        }

        [TestMethod()]
        public void ContainsKeyTest()
        {
            string[] keys = GetRandomKeys(15);
            foreach (string key in keys)
            {
                Assert.IsFalse(base.ContainsKey(key));
                base.Add(CreateCacheItem(key, null));
            }
            foreach (string key in keys)
            {
                Assert.IsTrue(base.ContainsKey(key));
                base.Remove(key);
                Assert.IsFalse(base.ContainsKey(key));
            }
        }

        [TestMethod()]
        public void CopyToTest()
        {
            Populate();
            KeyValuePair<string, string>[] arr1 = new KeyValuePair<string, string>[base.Count],
                arr2 = new KeyValuePair<string, string>[base.Count];
            int i = 0;
            foreach (KeyValuePair<string, string> item in this)
                arr1[i++] = item;

            base.CopyTo(arr2, 0);
            Assert.IsTrue(arr1.SequenceEqual(arr2));
        }

        [TestMethod()]
        [DeploymentItem("Cache.dll")]
        public void EnumerateByKeyTest()
        {
            string[] keys = GetRandomKeys(1000);
            foreach (string key in keys)
                base.Add(CreateCacheItem(key, null));

            Array.Sort(keys);
            Assert.IsTrue(keys.SequenceEqual(base.EnumerateByKey().Select(ci => ci.Key)));
        }

        [TestMethod()]
        [DeploymentItem("Cache.dll")]
        public void EnumerateByRankTest()
        {
            float[] d = PopulateDoubles();
            Array.Sort(d);
            Assert.IsTrue(d.SequenceEqual(EnumerateRank()));
        }

        [TestMethod()]
        public void ExamineTest()
        {
            string key = "key", value = "";
            CacheItem item = CreateCacheItem(key, value);
            base.Add(item);
            float rank = item.Rank.Rank;
            string s;
            Assert.IsTrue(base.Examine(key, out s));
            Assert.AreEqual(value, s);
            Assert.AreEqual(rank, item.Rank.Rank);
        }

        [TestMethod()]
        [DeploymentItem("Cache.dll")]
        public void GetItemTest()
        {
            Assert.AreEqual(default(string), base.GetItem("unexistingKey"));

            foreach (string key in GetRandomKeys(45))
            {
                CacheItem item = CreateCacheItem(key, null);
                base.Add(item);
                Assert.AreEqual(item, base.GetItem(key));
            }
        }

        [TestMethod()]
        [DeploymentItem("Cache.dll")]
        public void OnItemRequestedTest()
        {
            CacheItem item = CreateCacheItem("key", "");
            base.Add(item);
            DummyRank rank = (DummyRank)item.Rank;
            int hits = rank.Hits;
            base.OnItemRequested(item, DateTime.Now);
            Assert.AreEqual(hits + 1, rank.Hits);
        }

        //[TestMethod()]
        //[DeploymentItem("Cache.dll")]
        //public void OnItemsRemovedTest()
        //{
        //    OnItemsRemovedTestHelper<GenericParameterHelper, GenericParameterHelper>();
        //}

        [TestMethod()]
        [DeploymentItem("Cache.dll")]
        public void RankChangedTest()
        {
            Populate();
            List<float> d = EnumerateRank().ToList();

            CacheItem item = CreateCacheItem("key", null);
            base.Add(item);
            float rank = 10000;
            ((DummyRank)item.Rank).SetRank(rank);
            base.RankChanged(item);

            d.Add(rank);
            d.Sort();
            Assert.IsTrue(d.SequenceEqual(EnumerateRank()));
        }

        [TestMethod()]
        public void RemovePairTest()
        {
            Populate();
            int count = base.Count;
            foreach (string key in base.Keys.ToArray())
            {
                CacheItem item = base.GetItem(key);
                Assert.IsFalse(base.Remove(new KeyValuePair<string, string>(key, item.Value + "+")));
                Assert.IsTrue(base.Remove(new KeyValuePair<string, string>(key, item.Value)));
                Assert.IsFalse(base.Remove(new KeyValuePair<string, string>(key, item.Value)));
                Assert.IsFalse(base.ContainsKey(key));
                Assert.AreEqual(--count, base.Count);
            }
        }

        [TestMethod()]
        public void RemoveTest()
        {
            Populate();
            string key = base.Keys.First();
            int count = base.Count;
            Assert.IsTrue(base.Remove(key));
            Assert.IsFalse(base.ContainsKey(key));
            Assert.AreEqual(count - 1, base.Count);

            Assert.IsFalse(base.Remove(key));
            Assert.AreEqual(count - 1, base.Count);
        }

        [TestMethod()]
        public void RemoveBottomTest()
        {
            List<float> d = new List<float>(PopulateDoubles());
            int n = d.Count / 2;
            base.RemoveBottom(n);
            d.Sort();
            d.RemoveRange(0, n);
            Assert.IsTrue(d.SequenceEqual(EnumerateRank()));
        }

        [TestMethod()]
        public void TryGetValueTest()
        {
            string s;
            Assert.IsFalse(base.TryGetValue("unexistingKey", out s));

            Populate();
            string key = base.Keys.First();
            CacheItem item = base.GetItem(key);
            int hits = ((DummyRank)item.Rank).Hits;
            Assert.IsTrue(base.TryGetValue(key, out s));
            Assert.AreEqual(item.Value, s);
            Assert.AreEqual(hits + 1, ((DummyRank)item.Rank).Hits);
        }

        //[TestMethod()]
        //public void CountTest()
        //{
        //    throw new NotImplementedException();
        //}

        [TestMethod()]
        public void ItemTest()
        {
            Populate();
            CacheItem item = base.GetItem(base.Keys.First());
            int hits = ((DummyRank)item.Rank).Hits;
            Assert.AreEqual(item.Value, base[item.Key]);
            Assert.AreEqual(hits + 1, ((DummyRank)item.Rank).Hits);
            base.Remove(item.Key);
            try
            {
                string s = base[item.Key];
                Assert.Fail();
            }
            catch (KeyNotFoundException) { }
        }

        [TestMethod()]
        public void KeysTest()
        {
            Populate();
            Assert.AreEqual(base.Count, base.Keys.Count);
            Assert.IsTrue(base.Keys.SequenceEqual(base.EnumerateByKey().Select(ci => ci.Key)));

            base.RemoveBottom(base.Count / 2);
            Assert.AreEqual(base.Count, Keys.Count);
            Assert.IsTrue(base.Keys.SequenceEqual(base.EnumerateByKey().Select(ci => ci.Key)));

            string key = this.First().Key;
            Assert.IsTrue(base.Keys.Contains(key));
            base.Remove(key);
            Assert.IsFalse(base.Keys.Contains(key));
        }

        //[TestMethod()]
        //public void RankerTest()
        //{
        //    throw new NotImplementedException();
        //}

        [TestMethod()]
        public void ValuesTest()
        {
            Populate();
            Assert.AreEqual(base.Count, base.Values.Count);
            Assert.IsTrue(base.Values.SequenceEqual(base.EnumerateByKey().Select(ci => ci.Value)));

            base.RemoveBottom(base.Count / 2);
            Assert.AreEqual(base.Count, base.Values.Count);
            Assert.IsTrue(base.Values.SequenceEqual(base.EnumerateByKey().Select(ci => ci.Value)));

            var pair = this.First();
            Assert.IsTrue(base.Values.Contains(pair.Value));
            base.Remove(pair);
            Assert.IsFalse(base.Values.Contains(pair.Value));
        }

        [TestMethod()]
        public void ItemsRemovedEventTest()
        {
            Populate();

            KeyValuePair<string, string> pair = this.First();
            base.Remove(pair.Key);
            Assert.AreEqual(1, _lastRemovedCount);
            Assert.AreEqual(pair, _lastRemovedItems.First());

            pair = this.First();
            base.Remove(pair);
            Assert.AreEqual(1, _lastRemovedCount);
            Assert.AreEqual(pair, _lastRemovedItems.First());

            int n = base.Count / 2;
            var items = new KeyValuePair<string, string>[n];
            var enumerator = base.EnumerateByRank().GetEnumerator();
            for (int i = 0; i < n; i++)
            {
                enumerator.MoveNext();
                items[i] = new KeyValuePair<string, string>(enumerator.Current.Key, enumerator.Current.Value);
            }
            RemoveBottom(n);
            Assert.AreEqual(n, _lastRemovedCount);
            Assert.IsTrue(items.SequenceEqual(_lastRemovedItems));

            n = base.Count;
            items = this.ToArray();
            base.Clear();
            Assert.AreEqual(n, _lastRemovedCount);
            Assert.IsTrue(items.SequenceEqual(_lastRemovedItems));

            float[] d = PopulateDoubles();
            float minRank = d.Average();
            items = (from ci in base.EnumerateByRank()
                     where ci.Rank.Rank < minRank
                     select new KeyValuePair<string, string>(ci.Key, ci.Value))
                    .ToArray();
            base.Clean(minRank);
            Assert.AreEqual(items.Length, _lastRemovedCount);
            Assert.IsTrue(items.SequenceEqual(_lastRemovedItems));
        }

        CacheItem CreateCacheItem(string key, string value)
        {
            return new CacheItem(key, value, Ranker.CreateRank());
        }

        string[] GetRandomKeys(int count)
        {
            string[] res = new string[count];
            Random rnd = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < count; i++)
                res[i] = rnd.Next().ToString();
            return res;
        }

        float[] GetRandomDoubles(int count)
        {
            float[] res = new float[count];
            Random rnd = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < count; i++)
                res[i] = (float)rnd.NextDouble();
            return res;
        }

        void Populate()
        {
            int count = DateTime.Now.Millisecond + 5;
            Random rnd = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < count; i++)
                base.Add(rnd.Next().ToString(), rnd.Next().ToString(), (float)rnd.NextDouble());
        }

        float[] PopulateDoubles()
        {
            int count = DateTime.Now.Millisecond + 5;
            float[] res = new float[count];
            Random rnd = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < count; i++)
            {
                res[i] = (float)rnd.NextDouble();
                base.Add(rnd.Next().ToString(), rnd.Next().ToString(), res[i]);
            }
            return res;
        }

        IEnumerable<float> EnumerateRank()
        {
            return base.EnumerateByRank().Select(ci => ci.Rank.Rank);
        }
    }
}
