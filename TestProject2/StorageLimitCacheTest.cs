﻿using Bruss;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Collections.Generic;

namespace TestProject2
{
    [TestClass()]
    public class StorageLimitCacheTest : StorageLimitCache<string, string>
    {
        [TestInitialize]
        public void Init()
        {
            base.MaxVolume = 1000000;
        }

        [TestCleanup]
        public void Cleanup()
        {
            base.Clear();
            Assert.AreEqual(0, base.Count);
            Assert.AreEqual(0, base.Volume);
        }

        public StorageLimitCacheTest() : base(new DummyRanker()) { }

        [TestMethod()]
        public void VolumeTest()
        {
            base.Clear();
            Assert.AreEqual(0, base.Volume);

            base.MaxVolume = 10000;
            int max = base.MaxVolume / 10;
            Random rnd = new Random();
            int volume;
            int key = 0;
            for (; ; )
            {
                volume = 3000 + key + base.Volume;// rnd.Next(0, max) + base.Volume;
                if (volume > base.MaxVolume)
                    break;
                base.Add((++key).ToString(), null, 1.0f, volume - base.Volume);
                Assert.AreEqual(volume, base.Volume);
            }

            CheckAddItem((++key).ToString(), 1.0f, volume - base.Volume);

            volume = base.Volume;
            foreach (var itm in base.EnumerateByKey().ToArray())
            {
                var item = (StorageLimitCacheItem)itm;
                volume -= item.Volume;
                base.Remove(item.Key);
                Assert.AreEqual(volume, base.Volume);
            }

        }

        [TestMethod()]
        public void MaxVolumeTest()
        {
            base.MaxVolume = 50000;
            Populate();
            int newVolume = base.Volume / 2;
            int expectedVolume = base.Volume;
            var items = base.EnumerateByKey().Cast<StorageLimitCacheItem>().ToArray();
            double expectedRank = items.Sum(itm => itm.Rank.Rank);

            float[,] T = OppositeKnapsack(items, MaxVolume - newVolume);
            var removedItems = GetItemsThrown(items, T);
            base.MaxVolume = newVolume;

            Assert.AreEqual(newVolume, base.MaxVolume);
            Assert.IsTrue(base.Volume <= base.MaxVolume);

            expectedRank -= T[T.GetLength(0) - 1, T.GetLength(1) - 1];
            T = null;
            float actualRank = base.EnumerateByKey().Sum(itm => itm.Rank.Rank);
            if (Math.Abs(expectedRank - actualRank) > .01)
                Assert.Fail("expected: {0} actual: {1}", expectedRank, actualRank);

            expectedVolume -= removedItems.Sum(itm => itm.Volume);
            Assert.AreEqual(expectedVolume, base.Volume);

            //Array.Sort(items, (x, y) => x.Key.CompareTo(y.Key));
            //Assert.IsTrue(base.Keys.SequenceEqual(items.Select(itm => itm.Key)));
        }

        [TestMethod()]
        public void AddTest1()
        {
            Assert.IsTrue(base.Add("1", null));
            Assert.AreEqual(1, base.Volume);
            Assert.IsTrue(base.Add("2", null, 2.3f));
            Assert.AreEqual(2, base.Volume);
        }

        [TestMethod()]
        public void AddTest()
        {
            base.MaxVolume = 10000;
            int i = Populate(1500);
            Random rnd = new Random();
            for (int j = i + 1000; i < j; i++)
                CheckAddItem(i.ToString(), (float)(100 * rnd.NextDouble()), 1500);
            Console.WriteLine("Leap 1 passed");

            base.Clear();
            i = Populate();
            for (int j = i + 1000; i < j; i++)
                CheckAddItem(i.ToString(), (float)(100 * rnd.NextDouble()), rnd.Next(1, 1000));
        }

        [TestMethod]
        public void MaximumRankTest()
        {
            //check that the cache keeps the maximum rank possible
            base.MaxVolume = 100;
            for (int i = 0; i < 10; i++)
                Assert.IsTrue(base.Add(i.ToString(), null, i + 1, 10));

            Assert.IsFalse(base.Add("too little rank", null, 0.5f, 10));

            float rank = 55;
            Assert.IsTrue(base.Add("high rank", null, 3, 10));
            Assert.AreEqual(rank + 2, base.EnumerateByKey().Sum(itm => itm.Rank.Rank));
        }

        [TestMethod]
        public void MinimumVolumeTest()
        {
            //the cache should hold the minimum volume, e.g. if 2 with the same rank differ in volume, then prefer the smaller one
            base.MaxVolume = 1000;
            int volume = 10;
            while (base.MaxVolume - ++volume > base.Volume)
                Assert.IsTrue(base.Add(volume.ToString(), null, 1, volume));

            base.MaxVolume = base.Volume + volume;
            Assert.IsFalse(base.Add("too big", null, 1, volume + 1));

            base.MaxVolume -= 2;
            Assert.IsTrue(base.Add("a little smaller", null, 1, volume - 1));
            Assert.IsFalse(base.ContainsKey(volume.ToString()));

        }

        [TestMethod]
        public void MaxRankMinVolumeTest()
        {
            Random rnd = new Random();
            base.MaxVolume = 10000;
            int i = Populate(1234);
            Console.WriteLine("different ranks, same volume");
            for (int j = i + 100; i < j; i++)
            {
                Console.Write("\r" + (i - (j - 100)).ToString());
                CheckAddItem(i.ToString(), (float)(1000 * rnd.NextDouble()), 1234);
            }

            base.Clear();
            i = Populate(56.0f);
            Console.WriteLine("\r\ndifferent volumes, same rank");
            for (int j = i + 100; i < j; i++)
            {
                Console.Write("\r" + (i - (j - 100)).ToString());
                CheckAddItem(i.ToString(), 56.0f, rnd.Next(1, 1000));
            }

            base.Clear();
            i = Populate();
            Console.WriteLine("\r\ndifferent volumes and different ranks");
            for (int j = i + 100; i < j; i++)
            {
                Console.Write("\r" + (i - (j - 100)).ToString());
                CheckAddItem(i.ToString(), (float)(1000 * rnd.NextDouble()), rnd.Next(1, 1000));
            }
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void NonpositiveVolumeTest()
        {
            base.Add("f", null, 0);
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void NegativeMaxVolumeTest()
        {
            base.MaxVolume = -1;
        }

        //float[,] Knapsack(StorageLimitCacheItem[] items, int maxVolume)
        //{
        //    //sort items by volume- prefer the smaller ones if there are 2 items with the same rank
        //    Array.Sort(items, (x, y) => x.Volume - y.Volume);

        //    float[,] T = new float[items.Length + 1, maxVolume + 1];

        //    for (int i = 1; i <= items.Length; i++)
        //    {
        //        var item = items[i - 1];//T is longer than items by 1
        //        for (int w = 1; w <= maxVolume; w++)
        //        {
        //            if (item.Volume > w) T[i, w] = T[i - 1, w];
        //            else
        //            {
        //                float with = T[i - 1, w - item.Volume] + item.Rank.Rank;
        //                float without = T[i - 1, w];
        //                T[i, w] = Math.Max(with, without);
        //            }
        //        }
        //    }

        //    return T;
        //}

        float[,] OppositeKnapsack(StorageLimitCacheItem[] items, int deviation)
        {
            Array.Sort(items, (x, y) => y.Volume - x.Volume);
            float[,] T = new float[items.Length + 1, deviation + 1];

            for (int i = 1; i <= deviation; i++)
                T[0, i] = float.PositiveInfinity;

            for (int i = 1; i <= items.Length; i++)
            {
                var item = items[i - 1];
                for (int w = 1; w <= deviation; w++)
                {
                    float with;
                    if (item.Volume >= w)
                        with = item.Rank.Rank;
                    else
                        with = T[i - 1, w - item.Volume] + item.Rank.Rank;
                    T[i, w] = Math.Min(with, T[i - 1, w]);
                }
            }

            return T;
        }

        StorageLimitCacheItem[] GetItemsThrown(StorageLimitCacheItem[] items, float[,] T)
        {
            var knapped = new List<StorageLimitCacheItem>();
            int i = T.GetLength(0) - 1,
                w = T.GetLength(1) - 1;
            //an item is knapped if T[i,w] < T[i-1,w]
            for (; i > 0; i--)
                if (T[i, w] < T[i - 1, w])
                {
                    knapped.Add(items[i - 1]);//T is longer than items by 1
                    //this item is built upon item in T[i-1, w-item.Volume]
                    w -= items[i - 1].Volume;
                    if (w <= 0)
                        break;
                }

            return knapped.ToArray();
        }

        int Populate()
        {
            Random rnd1 = new Random(DateTime.Now.Millisecond);
            Random rnd2 = new Random((int)DateTime.UtcNow.Ticks);
            int max = base.MaxVolume / 10;
            for (int i = 0; ; i++)
            {
                int volume = rnd1.Next(1, max);
                float rank = (float)rnd2.NextDouble();
                if (base.Volume + volume > base.MaxVolume)
                    return i;
                Assert.IsTrue(base.Add((i).ToString(), null, rank, volume));
            }
        }

        int Populate(int itemVolume)
        {
            int k = MaxVolume / itemVolume;
            Random rnd = new Random();
            for (int i = 0; i < k; i++)
                Assert.IsTrue(base.Add(i.ToString(), null, (float)(100 * rnd.NextDouble()), itemVolume));

            return k;
        }

        int Populate(float rank)
        {
            Random rnd = new Random();
            for (int i = 0; ; i++)
            {
                int volume = rnd.Next(1, 1000);
                if (base.MaxVolume - volume < base.Volume)
                    return i;
                Assert.IsTrue(base.Add(i.ToString(), null, rank, volume));
            }
        }

        void CheckAddItem(string key, float rank, int volume)
        {
            var newItem = new StorageLimitCacheItem(key, null, Ranker.CreateRank(rank), volume);
            var items = new StorageLimitCacheItem[base.Count + 1];
            int i = 0;
            foreach (var itm in base.EnumerateByKey())
                items[i++] = (StorageLimitCacheItem)itm;
            items[i] = newItem;

            int expectedVolume = base.Volume + volume;
            float expectedRank = items.Sum(itm => itm.Rank.Rank);
            if (volume + base.Volume > base.MaxVolume)
            {
                float[,] T = OppositeKnapsack(items, volume + base.Volume - base.MaxVolume);
                items = GetItemsThrown(items, T);
                expectedVolume -= items.Sum(itm => itm.Volume);
                expectedRank -= T[T.GetLength(0) - 1, T.GetLength(1) - 1];
            }

            base.Add(key, null, rank, volume);
            Assert.AreEqual(expectedVolume, base.Volume);
            float actualRank = base.EnumerateByKey().Sum(itm => itm.Rank.Rank);
            if (Math.Abs(expectedRank - actualRank) > .001f)
                Assert.Fail("expected: {0} actual: {1}", expectedRank, actualRank);

            //Array.Sort(items, (x, y) => x.Key.CompareTo(y.Key));
            //Assert.IsTrue(base.Keys.SequenceEqual(items.Select(itm => itm.Key)));
        }
    }
}
