﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace TestProject2
{
    [TestClass]
    public class KnapsackProblemTest
    {
        [TestMethod]
        public void KnapsackTest()
        {
            int[,] args ={
                            {30,0,40},
                            {30,0,400},
                            {30,0,8000},
                            {40,0,60},
                            {40,0,300},
                            {50,0,70},
                            {60,0,100},
                            {100,0,20},
                            {100,0,200},
                            {100,0,400},
                            {100,0,800},
                            {100,0,1200}
                        };
            Stopwatch watch = new Stopwatch();
            for (int i = 0; i < args.GetLength(0); i++)
            {
                int count = args[i, 0],
                    min = args[i, 1],
                    max = args[i, 2];
                var items = CreateItems(count, min, max);
                int weight = (int)(items.Sum(itm => itm.Weight) * 0.6);
                Console.WriteLine();
                Console.WriteLine("Leap {0}: {1} items, from {2} to {3}. Weight: {4}. Necassary steps: {5:p}", i, count, min, max, weight, (float)GetNecassarySteps(count, weight) / (count * weight));
                watch.Reset();
                watch.Start();
                int a = KnapsackProblem.Knapsack(items, weight);
                watch.Stop();
                Console.WriteLine("Knapsack completed in {0} ms, result is {1}.", watch.ElapsedMilliseconds, a);

                watch.Reset();
                watch.Start();
                int b = KnapsackProblem.AdvancedKnapsack(items, weight);
                watch.Stop();
                Console.WriteLine("Advanced Knapsack completed in {0} ms, {1:p} steps, {2:p} necassary steps, result is {3}",
                    watch.ElapsedMilliseconds, (float)KnapsackProblem.steps / (count * weight), (float)KnapsackProblem.steps / GetNecassarySteps(count, weight), b);

                Assert.AreEqual(a, b);
            }
        }

        [TestMethod]
        public void OppositeKnapsackTest()
        {
            int maxWeight = 10000;
            var items = CreateItems(1000, 11, 50);
            int weight = items.Sum(itm => itm.Weight);
            int value = items.Sum(itm => itm.Value);

            int expected = KnapsackProblem.Knapsack(items, maxWeight);
            int actual = value - KnapsackProblem.OppositeKnapsack(items, weight - maxWeight);
            Assert.AreEqual(expected, actual);

            actual = value - KnapsackProblem.OppositeKnapsack(items, weight);
            Assert.AreEqual(0, actual);

            //when there's no combination that reaches the deviation,
            //int.MaxValue is returned (meaning 'no answer')
            actual = KnapsackProblem.OppositeKnapsack(items, weight + 1);
            Assert.AreEqual(int.MaxValue, actual);

            items = CreateItems(1000, 100, 11, 50);
            weight = items.Sum(itm => itm.Weight);
            value = items.Sum(itm => itm.Value);
            expected = KnapsackProblem.Knapsack(items, maxWeight);
            actual = value - KnapsackProblem.OppositeKnapsack(items, weight - maxWeight);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void MinimalisticKnapsackTest()
        {
            var items = CreateItems(1000, 0, 100);
            int maxWeight = (int)(items.Sum(itm => itm.Weight) * 0.6);

            int expected = KnapsackProblem.Knapsack(items, maxWeight);
            int actual = KnapsackProblem.MinimalisticKnapsack(items, maxWeight);
            Assert.AreEqual(expected, actual);
        }

        KnapsackProblem.Item[] CreateItems(int count, int min, int max)
        {
            Random rnd1 = new Random(DateTime.Now.Millisecond);
            Random rnd2 = new Random((int)DateTime.UtcNow.Ticks);
            KnapsackProblem.Item[] res = new KnapsackProblem.Item[count];
            for (int i = 0; i < count; i++)
                res[i] = new KnapsackProblem.Item()
                {
                    Weight = rnd1.Next(min, max),
                    Value = rnd2.Next(min, max)
                };

            return res;
        }

        KnapsackProblem.Item[] CreateItems(int count, int value, int minWeight, int maxWeight)
        {
            Random rnd = new Random();
            KnapsackProblem.Item[] res = new KnapsackProblem.Item[count];
            for (int i = 0; i < count; i++)
                res[i] = new KnapsackProblem.Item()
                {
                    Value = value,
                    Weight = rnd.Next(minWeight, maxWeight)
                };

            return res;
        }

        int GetNecassarySteps(int items, int weight)
        {
            int n = 1;
            int i = 0, sum = 0;
            for (; i < items && n * 2 < weight; i++)
                sum += n = n * 2;
            sum += weight * (items - i);
            return sum;
        }
    }
}
