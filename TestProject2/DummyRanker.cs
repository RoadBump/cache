﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bruss;

namespace TestProject2
{
    public class DummyRanker : IRanker
    {
        #region IRanker Members

        public IRank CreateRank()
        {
            return new DummyRank();
        }

        public IRank CreateRank(float rank)
        {
            DummyRank dr = new DummyRank();
            dr.SetRank(rank);
            return dr;
        }

        public void OnRequested(IRank item, DateTime requestTime)
        {
            DummyRank dr = (DummyRank)item;
            dr.SetRank(dr.Rank + 1);
            dr.Hits++;
        }

        #endregion
    }

    public class DummyRank : IRank
    {
        public int Hits { get; set; }

        float _rank;
        public float Rank
        {
            get { return _rank; }
        }

        public void SetRank(float rank)
        {
            _rank = rank;
        }
    }
}
