﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bruss;

namespace TestProject2
{
    class DummyTimedRanker : ITimedRanker
    {
        DummyRank _lastCreated;
        public DummyRank LastCreatedRank
        {
            get { return _lastCreated; }
        }

        #region ITimedRanker Members
        //since a 64 bit date/time value doesn't convert precisely into float,
        //set the instance's creation time as a starting point
        DateTime _creationTime = DateTime.Now;

        public float Translate(DateTime time)
        {
            return time.Ticks - _creationTime.Ticks;
        }

        public DateTime Translate(float rank)
        {
            return new DateTime(((long)rank) + _creationTime.Ticks);
        }

        #endregion

        #region IRanker Members

        public IRank CreateRank()
        {
            _lastCreated = new DummyRank();
            return _lastCreated;
        }

        public IRank CreateRank(float rank)
        {
            _lastCreated = new DummyRank();
            _lastCreated.SetRank(rank);
            return _lastCreated;
        }

        public void OnRequested(IRank item, DateTime requestTime)
        {
            DummyRank r = (DummyRank)item;
            float rank = Translate(requestTime + TimeSpan.FromMinutes(5));
            if (rank >= r.Rank) r.SetRank(rank);
            r.Hits++;
        }

        #endregion
    }
}
