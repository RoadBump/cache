﻿using Bruss;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
namespace TestProject2
{
    [TestClass()]
    public class SizeLimitCacheTest : SizeLimitCache<string, string>
    {
        public SizeLimitCacheTest() : base(new DummyRanker()) { }

        [TestCleanup]
        public void Cleanup()
        {
            Clear();
            MaxSize = int.MaxValue;
        }

        [TestMethod()]
        public void AddTest()
        {
            Assert.IsTrue(base.Add("key", "value"));
            Assert.IsFalse(base.Add("key", "value"));//or should it be an error?

            MaxSize = 100;
            FillToLimit();
            float minRank = base.EnumerateByRank().First().Rank.Rank;
            Assert.IsFalse(base.Add("#$%^", null, minRank - 1));
            Assert.IsTrue(base.Add("#$%^", null, minRank));
            Assert.IsTrue(base.Add("^%$#&", null, minRank + 1));
            Assert.AreEqual(MaxSize, Count);

            //in the concrete ranker the default rank is 0
            base.RemoveBottom(1);
            base.Add("&^%$*", null, -100);
            Assert.IsTrue(base.Add("*!@$", null));
            Assert.IsTrue(base.Add("$763$", null));
            base.Add("#45^%", null, 100);
            Assert.IsFalse(base.Add("%$#897", null));
        }

        [TestMethod()]
        public void MaxSizeTest()
        {
            MaxSize = 100;
            for (int i = 0; i < 200; i++)
                base.Add(i.ToString(), null);
            Assert.AreEqual(MaxSize, Count);

            MaxSize = 50;
            Assert.AreEqual(MaxSize, Count);
        }

        void FillToLimit()
        {
            int key = 1;
            Random rnd = new Random();
            while (Count < MaxSize)
            {
                while (ContainsKey(key.ToString())) key += 1000;
                Add(key.ToString(), null, (float)rnd.NextDouble());
            }
        }
    }
}
